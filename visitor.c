//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include <stdlib.h>

#include "visitor.h"

void ast_visitor_preorder_accept(const struct ast_node* node,
                                 const struct ast_visitor* visitor, void *arg) {
    int i;
    switch (node->type) {
        case INT:
            visitor->visit_int(&node->node.int_v, arg);
            break;
        case FLOAT:
            visitor->visit_float(&node->node.float_v, arg);
            break;
        case STRING:
            visitor->visit_string(&node->node.str, arg);
            break;
        case ADD:
            visitor->visit_add(&node->node.op, arg);
            ast_visitor_preorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_preorder_accept(node->node.op.right, visitor, arg);
            break;
        case SUB:
            visitor->visit_sub(&node->node.op, arg);
            ast_visitor_preorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_preorder_accept(node->node.op.right, visitor, arg);
            break;
        case MUL:
            visitor->visit_mul(&node->node.op, arg);
            ast_visitor_preorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_preorder_accept(node->node.op.right, visitor, arg);
            break;
        case DIV:
            visitor->visit_div(&node->node.op, arg);
            ast_visitor_preorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_preorder_accept(node->node.op.right, visitor, arg);
            break;
        case VAR:
            visitor->visit_var(&node->node.var, arg);
            break;
        case ASSIGN:
            visitor->visit_assign(&node->node.assign, arg);
            ast_visitor_preorder_accept(node->node.assign.var, visitor, arg);
            ast_visitor_preorder_accept(node->node.assign.val, visitor, arg);
            break;
        case STATEMENTS:
            visitor->visit_statements(&node->node.statements, arg);
            for (i = 0; i < node->node.statements.size; ++i)
                ast_visitor_preorder_accept(node->node.statements.list[i], visitor, arg);
            break;
    }
}

void ast_visitor_inorder_accept(const struct ast_node* node,
                                const struct ast_visitor* visitor, void *arg) {
    int i;
    switch (node->type) {
        case INT:
            visitor->visit_int(&node->node.int_v, arg);
            break;
        case FLOAT:
            visitor->visit_float(&node->node.float_v, arg);
            break;
        case STRING:
            visitor->visit_string(&node->node.str, arg);
            break;
        case ADD:
            ast_visitor_inorder_accept(node->node.op.left, visitor, arg);
            visitor->visit_add(&node->node.op, arg);
            ast_visitor_inorder_accept(node->node.op.right, visitor, arg);
            break;
        case SUB:
            ast_visitor_inorder_accept(node->node.op.left, visitor, arg);
            visitor->visit_sub(&node->node.op, arg);
            ast_visitor_inorder_accept(node->node.op.right, visitor, arg);
            break;
        case MUL:
            ast_visitor_inorder_accept(node->node.op.left, visitor, arg);
            visitor->visit_mul(&node->node.op, arg);
            ast_visitor_inorder_accept(node->node.op.right, visitor, arg);
            break;
        case DIV:
            ast_visitor_inorder_accept(node->node.op.left, visitor, arg);
            visitor->visit_div(&node->node.op, arg);
            ast_visitor_inorder_accept(node->node.op.right, visitor, arg);
            break;
        case VAR:
            visitor->visit_var(&node->node.var, arg);
            break;
        case ASSIGN:
            ast_visitor_inorder_accept(node->node.assign.var, visitor, arg);
            visitor->visit_assign(&node->node.assign, arg);
            ast_visitor_inorder_accept(node->node.assign.val, visitor, arg);
            break;
        case STATEMENTS:
            visitor->visit_statements(&node->node.statements, arg);
            for (i = 0; i < node->node.statements.size; ++i)
                ast_visitor_inorder_accept(node->node.statements.list[i], visitor, arg);
            break;
    }
}

void ast_visitor_postorder_accept(const struct ast_node* node,
                                  const struct ast_visitor* visitor, void *arg) {
    int i;
    switch (node->type) {
        case INT:
            visitor->visit_int(&node->node.int_v, arg);
            break;
        case FLOAT:
            visitor->visit_float(&node->node.float_v, arg);
            break;
        case STRING:
            visitor->visit_string(&node->node.str, arg);
            break;
        case ADD:
            ast_visitor_postorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_postorder_accept(node->node.op.right, visitor, arg);
            visitor->visit_add(&node->node.op, arg);
            break;
        case SUB:
            ast_visitor_postorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_postorder_accept(node->node.op.right, visitor, arg);
            visitor->visit_sub(&node->node.op, arg);
            break;
        case MUL:
            ast_visitor_postorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_postorder_accept(node->node.op.right, visitor, arg);
            visitor->visit_mul(&node->node.op, arg);
            break;
        case DIV:
            ast_visitor_postorder_accept(node->node.op.left, visitor, arg);
            ast_visitor_postorder_accept(node->node.op.right, visitor, arg);
            visitor->visit_div(&node->node.op, arg);
            break;
        case VAR:
            visitor->visit_var(&node->node.var, arg);
            break;
        case ASSIGN:
            ast_visitor_postorder_accept(node->node.assign.var, visitor, arg);
            ast_visitor_postorder_accept(node->node.assign.val, visitor, arg);
            visitor->visit_assign(&node->node.assign, arg);
            break;
        case STATEMENTS:
            for (i = 0; i < node->node.statements.size; ++i)
                ast_visitor_postorder_accept(node->node.statements.list[i], visitor, arg);
            visitor->visit_statements(&node->node.statements, arg);
            break;
    }
}
