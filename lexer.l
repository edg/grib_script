/*
Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 2 of the License or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>
*/

%{
#include "ast.h"
#include "parser.h"

#include <stdio.h>
%}

%option outfile="lexer.c" header-file="lexer.h"
%option warn
%option reentrant noyywrap never-interactive nounistd
%option noinput nounput
%option bison-bridge

%%

[ \r\n\t]   { continue; }
[0-9]+      { sscanf(yytext, "%d", &yylval->int_v); return TOKEN_INT; }
[0-9]+[.][0-9]*  {sscanf(yytext, "%f", &yylval->float_v); return TOKEN_FLOAT; }
"{"         { return TOKEN_L_CURLYPAREN; }
"}"         { return TOKEN_R_CURLYPAREN; }
"+"         { return TOKEN_ADD; }
"-"         { return TOKEN_SUB; }
"*"         { return TOKEN_MUL; }
"/"         { return TOKEN_DIV; }
"("         { return TOKEN_L_ROUNDPAREN; }
")"         { return TOKEN_R_ROUNDPAREN; }
"="         { return TOKEN_EQUALS; }
[a-zA-Z_][a-zA-Z0-9_]* { yylval->string = strdup(yytext); return TOKEN_IDENTIFIER; }
";"         { return TOKEN_SEMICOLON; }

.           { yylval->string = strdup(yytext); return TOKEN_UNKNOWN; }

%%

void yyerror(struct ast_node** node, yyscan_t scanner, const char* msg) {
    fprintf(stderr, "Error: %s\n", msg);
}
