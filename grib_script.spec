Name:           grib_script
Version:        0.1
Release:        1%{?dist}
Summary:        Interpreter for grib_script language

License:        GPLv2
URL:            https://gitlab.com/edg/grib_script
Source0:        https://gitlab.com/edg/grib_script/-/archive/master/grib_script-master.tar.gz

BuildRequires:  gcc
BuildRequires:  libtool
BuildRequires:  valgrind

%description
Interpreter for grib_script language.


%prep
%autosetup -n grib_script-master


%build
autoreconf -ifv
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install

%check
make check
make check-valgrind


%files
%{_bindir}/grib_script_graph
%license LICENSE
%doc README.md



%changelog
* Fri Oct  4 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
- 
