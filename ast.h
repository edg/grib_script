//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef AST_H
#define AST_H

#include <stdlib.h>

/**
 * @file ast.h
 * @brief AST header file.
 */


/* Enum for AST node types. */
enum ast_node_type {
    INT,
    FLOAT,
    STRING,
    ADD,
    SUB,
    MUL,
    DIV,
    VAR,
    ASSIGN,
    STATEMENTS,
};

struct ast_node;

/**
 * @brief AST node for statement list.
 *
 * This data structure was implemented as dynamic array instead as linked list.
 * A linked list is more efficient on insert and remove, but:
 * - There's need to remove a specific item in the list.
 * - To reduce the number of realloc on insert items, we can allocate the
 *   memory for N items.
 * Moreover, a linked list would be more complicated to handle :)
 */
struct ast_node_statements {
    size_t size;
    struct ast_node** list;
};

/* AST node for integer. */
struct ast_node_int {
    int value;
};

/* AST node for float. */
struct ast_node_float {
    int value;
};

/* AST node for string. */
struct ast_node_string {
    char* value;
};

/* AST node for binary operation. */
struct ast_node_op {
    struct ast_node* left;
    struct ast_node* right;
};

/* AST node for variable. */
struct ast_node_var {
    char* name;
};

/* AST node for assignment. */
struct ast_node_assign {
    struct ast_node* var;
    struct ast_node* val;
};

/* Generic AST node. */
struct ast_node {
    enum ast_node_type type;
    union {
        struct ast_node_int int_v;
        struct ast_node_float float_v;
        struct ast_node_string str;
        struct ast_node_op op;
        struct ast_node_var var;
        struct ast_node_assign assign;
        struct ast_node_statements statements;
    } node;
};

/* Create AST node for statement list. */
struct ast_node* ast_node_create_statements();
/* Add AST node to statement list. */
void ast_node_statements_add(struct ast_node_statements* head, struct ast_node* item);

/* Create AST node for integer. */
struct ast_node* ast_node_create_int(int value);
/* Create AST node for float. */
struct ast_node* ast_node_create_float(float value);
/* Create AST node for string. */
struct ast_node* ast_node_create_string(const char* str);
/* Create AST node for addition. */
struct ast_node* ast_node_create_add(struct ast_node* left, struct ast_node* right);
/* Create AST node for subtraction. */
struct ast_node* ast_node_create_sub(struct ast_node* left, struct ast_node* right);
/* Create AST node for multiplication */
struct ast_node* ast_node_create_mul(struct ast_node* left, struct ast_node* right);
/* Create AST node for division */
struct ast_node* ast_node_create_div(struct ast_node* left, struct ast_node* right);
/* Create AST node per variable */
struct ast_node* ast_node_create_var(const char* name);
/* Create AST node for assignment */
struct ast_node* ast_node_create_assign(struct ast_node* var, struct ast_node* val);
/* Dealloc AST memory */
void ast_node_destroy(struct ast_node* node);

#endif
