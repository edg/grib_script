/*
Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 2 of the License or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see <http://www.gnu.org/licenses/>
*/

%{
#include <stdlib.h>
#include "ast.h"
#include "parser.h"
#include "lexer.h"

void yyerror(struct ast_node** node, yyscan_t scanner, const char* msg);

%}

%code requires {

#include <stdio.h>
#include "ast.h"
typedef void* yyscan_t;
struct ast_node* parse_string(const char* str);
struct ast_node* parse_file(FILE* f);

}

%output "parser.c"
%defines "parser.h"

%define api.pure
%lex-param { yyscan_t scanner }
%parse-param { struct ast_node** node }
%parse-param { yyscan_t scanner }

%union {
    char* string;
    int int_v;
    float float_v;
    struct ast_node *node;
}

%token TOKEN_ADD
%token TOKEN_SUB
%token TOKEN_MUL
%token TOKEN_DIV
%token TOKEN_SEMICOLON
%token <int_v> TOKEN_INT
%token <float_v> TOKEN_FLOAT
%token <string> TOKEN_IDENTIFIER
%token TOKEN_EQUALS
%token TOKEN_L_ROUNDPAREN
%token TOKEN_R_ROUNDPAREN
%token TOKEN_L_CURLYPAREN
%token TOKEN_R_CURLYPAREN
%token <string> TOKEN_UNKNOWN

%type <node> statements;
%type <node> statement;
%type <node> expression;
%type <node> term;
%type <node> factor;
%type <node> assign;

%destructor { } <*>
%destructor { free($$); } <string>
%destructor { ast_node_destroy($$); } <node>

%%

input: statements { *node = $1; } ;

statements: %empty { $$ = ast_node_create_statements(); } |
            statements statement { ast_node_statements_add(&$1->node.statements, $2); $$ = $1; } ;

statement: assign TOKEN_SEMICOLON { $$ = $1; } |
           TOKEN_L_CURLYPAREN statements TOKEN_R_CURLYPAREN { $$ = $2; };

assign:
      expression { $$ = $1; } |
      TOKEN_IDENTIFIER[L] TOKEN_EQUALS expression[R] { $$ = ast_node_create_assign(ast_node_create_var($L), $R); free($L); } ;

expression:
          term { $$ = $1; } |
          expression[L] TOKEN_ADD term[R] { $$ = ast_node_create_add($L, $R); } |
          expression[L] TOKEN_SUB term[R] { $$ = ast_node_create_sub($L, $R); } ;

term:
    factor { $$ = $1; } |
    term[L] TOKEN_MUL factor[R] { $$ = ast_node_create_mul($L, $R); } |
    term[L] TOKEN_DIV factor[R] { $$ = ast_node_create_div($L, $R); } ;

factor:
      TOKEN_INT { $$ = ast_node_create_int($1); } |
      TOKEN_FLOAT { $$ = ast_node_create_float($1); } |
      TOKEN_IDENTIFIER { $$ = ast_node_create_var($1) ; free($1); } |
      TOKEN_L_ROUNDPAREN expression[E] TOKEN_R_ROUNDPAREN  { $$ = $E; } ;

%%

struct ast_node* parse_file(FILE* f) {
    struct ast_node* node = NULL;
    yyscan_t scanner;

    if (yylex_init(&scanner)) {
        goto cleanup_scanner;
    }

    yyset_in(f, scanner);

    int res = yyparse(&node, scanner);
    if (res != 0) {
        if (node != NULL)
            ast_node_destroy(node);
        node = NULL;
    }

cleanup_scanner:
    yylex_destroy(scanner);
    return node;
}

struct ast_node* parse_string(const char* str) {
    struct ast_node* node = NULL;
    yyscan_t scanner;
    YY_BUFFER_STATE state;

    if (yylex_init(&scanner)) {
        goto cleanup_scanner;
    }

    state = yy_scan_string(str, scanner);

    int res = yyparse(&node, scanner);
    if (res != 0) {
        if (node != NULL)
            ast_node_destroy(node);
        node = NULL;
    }

    yy_delete_buffer(state, scanner);
cleanup_scanner:
    yylex_destroy(scanner);
    return node;
}
