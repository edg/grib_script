//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef VISITOR_H
#define VISITOR_H

#include "ast.h"

/**
 * @file visitor.h
 * @brief AST visitor.
 */

/* AST visitor */
struct ast_visitor {
    void (*visit_int)(const struct ast_node_int*, void*);
    void (*visit_float)(const struct ast_node_float*, void*);
    void (*visit_string)(const struct ast_node_string*, void*);
    void (*visit_add)(const struct ast_node_op*, void*);
    void (*visit_sub)(const struct ast_node_op*, void*);
    void (*visit_mul)(const struct ast_node_op*, void*);
    void (*visit_div)(const struct ast_node_op*, void*);
    void (*visit_var)(const struct ast_node_var*, void*);
    void (*visit_assign)(const struct ast_node_assign*, void*);
    void (*visit_statements)(const struct ast_node_statements*, void*);
};

/* Preorder visit */
void ast_visitor_preorder_accept(const struct ast_node* node,
                                 const struct ast_visitor* visitor, void *arg);

/* Inorder visit */
void ast_visitor_inorder_accept(const struct ast_node* node,
                                const struct ast_visitor* visitor, void *arg);
/* Postoder visit */
void ast_visitor_postorder_accept(const struct ast_node* node,
                                  const struct ast_visitor* visitor, void *arg);

#endif
