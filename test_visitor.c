//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include "visitor.h"

struct test_visitor_arg {
    enum ast_node_type node_list[1024];
    size_t index;
};

void test_visit_int(const struct ast_node_int* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = INT;
}

void test_visit_float(const struct ast_node_float* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = FLOAT;
}

void test_visit_string(const struct ast_node_string* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = STRING;
}

void test_visit_add(const struct ast_node_op* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = ADD;
}

void test_visit_sub(const struct ast_node_op* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = SUB;
}

void test_visit_mul(const struct ast_node_op* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = MUL;
}

void test_visit_div(const struct ast_node_op* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = DIV;
}

void test_visit_var(const struct ast_node_var* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = VAR;
}

void test_visit_assign(const struct ast_node_assign* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = ASSIGN;
}

void test_visit_statements(const struct ast_node_statements* node, void* arg) {
    struct test_visitor_arg* o = (struct test_visitor_arg*) arg;
    o->node_list[o->index++] = STATEMENTS;
}


struct ast_visitor test_visitor = {
    .visit_int = test_visit_int,
    .visit_float = test_visit_float,
    .visit_string = test_visit_string,
    .visit_add = test_visit_add,
    .visit_sub = test_visit_sub,
    .visit_mul = test_visit_mul,
    .visit_div = test_visit_div,
    .visit_var = test_visit_var,
    .visit_assign = test_visit_assign,
    .visit_statements = test_visit_statements,
};

#define assert_node_list_eq(list1, list1_size, list2) \
    do { \
        int i = 0; \
        size_t list2_size = sizeof(list2)/sizeof(enum ast_node_type); \
        ck_assert_int_eq(list1_size, list2_size); \
        for (i = 0; i < list1_size; ++i) \
        ck_assert_int_eq(list1[i], list2[i]); \
    } while(0)


START_TEST(test_visitor_01) {
    struct ast_node* node = ast_node_create_statements();
    struct test_visitor_arg list;

    enum ast_node_type postorder_list[] = {
        INT, FLOAT, ADD,
        VAR, INT, SUB,
        VAR, STRING, ASSIGN,
        INT, INT, MUL,
        INT, INT, DIV,
        STATEMENTS,
    };
    enum ast_node_type preorder_list[] = {
        STATEMENTS,
        ADD, INT, FLOAT,
        SUB, VAR, INT,
        ASSIGN, VAR, STRING,
        MUL, INT, INT,
        DIV, INT, INT,
    };
    enum ast_node_type inorder_list[] = {
        STATEMENTS,
        INT, ADD, FLOAT,
        VAR, SUB, INT,
        VAR, ASSIGN, STRING,
        INT, MUL, INT,
        INT, DIV, INT,
    };

    ast_node_statements_add(&node->node.statements,
                            ast_node_create_add(
                                ast_node_create_int(1),
                                ast_node_create_float(1.5)));
    ast_node_statements_add(&node->node.statements,
                            ast_node_create_sub(
                                ast_node_create_var("x"),
                                ast_node_create_int(2)));
    ast_node_statements_add(&node->node.statements,
                            ast_node_create_assign(
                                ast_node_create_var("x"),
                                ast_node_create_string("y")));
    ast_node_statements_add(&node->node.statements,
                            ast_node_create_mul(
                                ast_node_create_int(1),
                                ast_node_create_int(2)));
    ast_node_statements_add(&node->node.statements,
                            ast_node_create_div(
                                ast_node_create_int(1),
                                ast_node_create_int(2)));

    list.index = 0;
    ast_visitor_postorder_accept(node, &test_visitor, &list);
    assert_node_list_eq(list.node_list, list.index, postorder_list);

    list.index = 0;
    ast_visitor_preorder_accept(node, &test_visitor, &list);
    assert_node_list_eq(list.node_list, list.index, preorder_list);

    list.index = 0;
    ast_visitor_inorder_accept(node, &test_visitor, &list);
    assert_node_list_eq(list.node_list, list.index, inorder_list);

    ast_node_destroy(node);
}
END_TEST

Suite* parser_suite(void) {
    Suite* s;
    TCase* tc_core;

    s = suite_create("Visitor");
    tc_core = tcase_create("Core");
    tcase_add_test(tc_core, test_visitor_01);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    Suite* s;
    SRunner* sr;
    int nfailed;

    s = parser_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    nfailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (nfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
