//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include "parser.h"
#include "lexer.h"
#include "visitor.h"
#include "graph.h"

#include <stdlib.h>
#include <stdio.h>


void printer_visit_int(const struct ast_node* node, void* arg) {
    printf("Number %d\n", node->node.int_v.value);
}

void printer_visit_add(const struct ast_node* node, void* arg) {
    printf("Add\n");
}

void printer_visit_sub(const struct ast_node* node, void* arg) {
    printf("Sub\n");
}

void printer_visit_mul(const struct ast_node* node, void* arg) {
    printf("Mul\n");
}

void printer_visit_div(const struct ast_node* node, void* arg) {
    printf("Div\n");
}

struct calc_visitor_arg {
    int stack[30];
    int index;
};

void calc_visit_int(const struct ast_node* node, void* arg) {
    struct calc_visitor_arg* a = (struct calc_visitor_arg*) arg;
    int i = ++(a->index);
    a->stack[i] = node->node.int_v.value;
}

void calc_visit_add(const struct ast_node* node, void* arg) {
    struct calc_visitor_arg* a = (struct calc_visitor_arg*) arg;
    int i = a->index;
    int x = a->stack[i];
    int y = a->stack[i-1];
    a->stack[i-1] = x + y;
    a->index--;
}

void calc_visit_sub(const struct ast_node* node, void* arg) {
    struct calc_visitor_arg* a = (struct calc_visitor_arg*) arg;
    int i = a->index;
    int x = a->stack[i];
    int y = a->stack[i-1];
    a->stack[i-1] = y - x;
    a->index--;
}

void calc_visit_mul(const struct ast_node* node, void* arg) {
    struct calc_visitor_arg* a = (struct calc_visitor_arg*) arg;
    int i = a->index;
    int x = a->stack[i];
    int y = a->stack[i-1];
    a->stack[i-1] = y * x;
    a->index--;
}

void calc_visit_div(const struct ast_node* node, void* arg) {
    struct calc_visitor_arg* a = (struct calc_visitor_arg*) arg;
    int i = a->index;
    int x = a->stack[i];
    int y = a->stack[i-1];
    a->stack[i-1] = y / x;
    a->index--;
}


#if YYDEBUG
extern int yydebug;
#endif

int main(int argc, const char** argv)
{
#if YYDEBUG
    yydebug = 1;
#endif
    int exit_status = 0;
    struct ast_node* node = parse_file(stdin);
    if (node != NULL)
        graph_print_file(node, stdout);
    else
        exit_status = 1;

    ast_node_destroy(node);
    return exit_status;

    /*
    if (node == NULL)
        return 1;

    struct visitor printer_visitor = {
        .visit_int = printer_visit_int,
        .visit_add = printer_visit_add,
        .visit_sub = printer_visit_sub,
        .visit_mul = printer_visit_mul,
        .visit_div = printer_visit_div,
    };
    postorder_accept(node, &printer_visitor, NULL);

    struct calc_visitor_arg arg = {
        .stack = { 0 },
        .index = -1,
    };
    struct visitor calc_visitor = {
        .visit_int = calc_visit_int,
        .visit_add = calc_visit_add,
        .visit_sub = calc_visit_sub,
        .visit_mul = calc_visit_mul,
        .visit_div = calc_visit_div,
    };

    postorder_accept(node, &calc_visitor, &arg);
    printf("Result: %d\n", arg.stack[0]);
    */
}
