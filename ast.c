//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include "ast.h"

#include <stdlib.h>
#include <string.h>


struct ast_node* ast_node_create_statements() {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = STATEMENTS;
    node->node.statements.size = 0;
    node->node.statements.list = NULL;
    return node;
}

void ast_node_statements_add(struct ast_node_statements* head, struct ast_node* item) {
    head->size++;
    head->list = realloc(head->list, head->size*sizeof(struct ast_node));
    head->list[head->size - 1] = item;
}

struct ast_node* ast_node_create_int(int value) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = INT;
    node->node.int_v.value = value;
    return node;
}

struct ast_node* ast_node_create_float(float value) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = FLOAT;
    node->node.float_v.value = value;
    return node;
}

struct ast_node* ast_node_create_string(const char* str) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = STRING;
    node->node.str.value = strdup(str);
    return node;
}

struct ast_node* ast_node_create_add(struct ast_node* left, struct ast_node* right) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = ADD;
    node->node.op.left = left;
    node->node.op.right = right;
    return node;
}

struct ast_node* ast_node_create_sub(struct ast_node* left, struct ast_node* right) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = SUB;
    node->node.op.left = left;
    node->node.op.right = right;
    return node;
}

struct ast_node* ast_node_create_mul(struct ast_node* left, struct ast_node* right) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = MUL;
    node->node.op.left = left;
    node->node.op.right = right;
    return node;
}

struct ast_node* ast_node_create_div(struct ast_node* left, struct ast_node* right) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = DIV;
    node->node.op.left = left;
    node->node.op.right = right;
    return node;
}

struct ast_node* ast_node_create_var(const char* name) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = VAR;
    node->node.var.name = strdup(name);
    return node;
}

struct ast_node* ast_node_create_assign(struct ast_node* var, struct ast_node* val) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = ASSIGN;
    node->node.assign.var = var;
    node->node.assign.val = val;
    return node;
}

void ast_node_destroy(struct ast_node* node) {
    int i;

    if (node == NULL)
        return;

    switch (node->type) {
        case INT:
        case FLOAT:
            free(node);
            break;
        case STRING:
            free(node->node.str.value);
            free(node);
            break;
        case ADD:
        case SUB:
        case MUL:
        case DIV:
            ast_node_destroy(node->node.op.left);
            ast_node_destroy(node->node.op.right);
            free(node);
            break;
        case VAR:
            free(node->node.var.name);
            free(node);
            break;
        case ASSIGN:
            ast_node_destroy(node->node.assign.var);
            ast_node_destroy(node->node.assign.val);
            free(node);
            break;
        case STATEMENTS:
            for (i = 0; i < node->node.statements.size; ++i)
                ast_node_destroy(node->node.statements.list[i]);
            free(node->node.statements.list);
            free(node);
            break;
    }
}
