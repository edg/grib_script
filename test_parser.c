//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include <stdlib.h>
#include <stdbool.h>

#include <check.h>

#include "parser.h"


// Check a simple operation
START_TEST(test_parse_01) {
    struct ast_node* node = parse_string("1+2-3.5;");
    ck_assert(node != NULL);
    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 1);
    ck_assert_int_eq(node->node.statements.list[0]->type, SUB);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->type, FLOAT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.float_v.value, 3.5);
    ast_node_destroy(node);
}
END_TEST

// Check invalid strings
START_TEST(test_parse_02) {
    ck_assert(parse_string("1+1-") == NULL);
    ck_assert(parse_string("1+1-3(") == NULL);
    ck_assert(parse_string("3c") == NULL);
    ck_assert(parse_string("1+1") == NULL);
}
END_TEST

// Check operation order
START_TEST(test_parse_03) {
    struct ast_node* node = parse_string("1+2*3;");
    ck_assert(node != NULL);
    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 1);
    ck_assert_int_eq(node->node.statements.list[0]->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->type, MUL);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.op.left->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.op.right->node.int_v.value, 3);
    ast_node_destroy(node);
}
END_TEST

// Check assignment
START_TEST(test_parse_04) {
    struct ast_node* node = parse_string("y=1+2*3;");
    ck_assert(node != NULL);
    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 1);
    ck_assert_int_eq(node->node.statements.list[0]->type, ASSIGN);
    ck_assert_int_eq(node->node.statements.list[0]->node.assign.var->type, VAR);
    ck_assert_str_eq(node->node.statements.list[0]->node.assign.var->node.var.name, "y");
    ck_assert_int_eq(node->node.statements.list[0]->node.assign.val->type, ADD);
    ast_node_destroy(node);
}
END_TEST

// Check operation order with parentheses
START_TEST(test_parse_05) {
    struct ast_node* node = parse_string("(1+2)*3;");
    ck_assert(node != NULL);
    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 1);
    ck_assert_int_eq(node->node.statements.list[0]->type, MUL);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.int_v.value, 3);
    ast_node_destroy(node);
}
END_TEST

// Check a simple operation with spaces
START_TEST(test_parse_06) {
    struct ast_node* node = parse_string("1 + 2-3;");
    ck_assert(node != NULL);
    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 1);
    ck_assert_int_eq(node->node.statements.list[0]->type, SUB);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.int_v.value, 3);
    ast_node_destroy(node);
}
END_TEST

// Check multiple statements
START_TEST(test_parse_07) {
    struct ast_node* node = parse_string("1+2-3; 4+5*6;");
    ck_assert(node != NULL);

    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 2);
    ck_assert_int_eq(node->node.statements.list[0]->type, SUB);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.left->node.op.right->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.op.right->node.int_v.value, 3);

    ck_assert_int_eq(node->node.statements.list[1]->type, ADD);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.left->node.int_v.value, 4);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->type, MUL);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.left->node.int_v.value, 5);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.right->node.int_v.value, 6);

    ast_node_destroy(node);
}
END_TEST

// Check statements with curly braces
START_TEST(test_parse_08) {
    struct ast_node* node = parse_string("{ 1+2-3; } 4+5*6;");
    ck_assert(node != NULL);

    ck_assert_int_eq(node->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.size, 2);
    ck_assert_int_eq(node->node.statements.list[0]->type, STATEMENTS);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->type, SUB);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.left->type, ADD);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.left->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.left->node.op.left->node.int_v.value, 1);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.left->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.left->node.op.right->node.int_v.value, 2);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[0]->node.statements.list[0]->node.op.right->node.int_v.value, 3);

    ck_assert_int_eq(node->node.statements.list[1]->type, ADD);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.left->node.int_v.value, 4);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->type, MUL);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.left->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.left->node.int_v.value, 5);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.right->type, INT);
    ck_assert_int_eq(node->node.statements.list[1]->node.op.right->node.op.right->node.int_v.value, 6);

    ast_node_destroy(node);
}
END_TEST

struct testfile {
    const char* path;
    bool is_good;
};

void check_file(const struct testfile* testfile) {
    FILE* file;
    struct ast_node* node;

    file = fopen(testfile->path, "r");
    ck_assert(file != NULL);
    node = parse_file(file);
    if (testfile->is_good)
        ck_assert(node != NULL);
    else
        ck_assert(node == NULL);

    ast_node_destroy(node);
    fclose(file);
}

START_TEST(test_parse_09) {
    struct testfile f = { .path =  "testdata/0001.good.gribscript", .is_good = true };
    check_file(&f);
}
END_TEST

START_TEST(test_parse_10) {
    struct testfile f = { .path =  "testdata/0001.bad.gribscript", .is_good = false };
    check_file(&f);
}
END_TEST

START_TEST(test_parse_11) {
    struct testfile f = { .path =  "testdata/0002.bad.gribscript", .is_good = false };
    check_file(&f);
}
END_TEST

START_TEST(test_parse_12) {
    struct testfile f = { .path =  "testdata/0003.bad.gribscript", .is_good = false };
    check_file(&f);
}
END_TEST

START_TEST(test_parse_13) {
    struct testfile f = { .path =  "testdata/0004.bad.gribscript", .is_good = false };
    check_file(&f);
}
END_TEST


Suite* parser_suite(void) {
    Suite* s;
    TCase* tc_core;

    s = suite_create("Parser");
    tc_core = tcase_create("Core");
    tcase_add_test(tc_core, test_parse_01);
    tcase_add_test(tc_core, test_parse_02);
    tcase_add_test(tc_core, test_parse_03);
    tcase_add_test(tc_core, test_parse_04);
    tcase_add_test(tc_core, test_parse_05);
    tcase_add_test(tc_core, test_parse_06);
    tcase_add_test(tc_core, test_parse_07);
    tcase_add_test(tc_core, test_parse_08);
    tcase_add_test(tc_core, test_parse_09);
    tcase_add_test(tc_core, test_parse_10);
    tcase_add_test(tc_core, test_parse_11);
    tcase_add_test(tc_core, test_parse_12);
    tcase_add_test(tc_core, test_parse_13);
    suite_add_tcase(s, tc_core);

    return s;
}


int main(void) {
    Suite* s;
    SRunner* sr;
    int nfailed;

    s = parser_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    nfailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (nfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
