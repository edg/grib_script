//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef UTILS_H
#define UTILS_H

struct stack_int {
    size_t count;
    int* values;
};

void stack_int_init(struct stack_int* s);

void stack_int_destroy(struct stack_int* s);

void stack_int_push(struct stack_int* s, int value);

int stack_int_pop(struct stack_int* s);

#endif
