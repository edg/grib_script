//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include <stdlib.h>
#include <check.h>

#include "ast.h"


/// Check destruction of NULL AST
START_TEST(test_ast_01) {
    ast_node_destroy(NULL);
}
END_TEST

Suite* parser_suite(void) {
    Suite* s;
    TCase* tc_core;

    s = suite_create("Ast");
    tc_core = tcase_create("Core");
    tcase_add_test(tc_core, test_ast_01);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    Suite* s;
    SRunner* sr;
    int nfailed;

    s = parser_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    nfailed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (nfailed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
