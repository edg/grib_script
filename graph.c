//  Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation, either version 2 of the License or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program.  If not, see <http://www.gnu.org/licenses/>

#include <stdio.h>
#include <stdlib.h>

#include "graph.h"
#include "ast.h"
#include "visitor.h"
#include "utils.h"
#include "parser.h"

struct graph_arg {
    FILE* outfile;
    int counter;
    struct stack_int stack;
};

void graph_visit_int(const struct ast_node_int* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"int (%d)\"];\n", o->counter, node->value);
    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_add(const struct ast_node_op* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"+\"];\n", o->counter);

    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_sub(const struct ast_node_op* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"-\"];\n", o->counter);

    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_mul(const struct ast_node_op* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"*\"];\n", o->counter);

    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_div(const struct ast_node_op* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"/\"];\n", o->counter);

    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_var(const struct ast_node_var* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"var (%s)\"];\n", o->counter, node->name);

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_assign(const struct ast_node_assign* node, void* arg) {
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"=\"];\n", o->counter);

    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));

    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

void graph_visit_statements(const struct ast_node_statements* node, void* arg) {
    int i;
    struct graph_arg* o = (struct graph_arg*) arg;
    fprintf(o->outfile, "  %d [label=\"statements\"];\n", o->counter);
    for (i = 0; i < node->size; i++)
        fprintf(o->outfile, "  %d -- %d;\n", o->counter, stack_int_pop(&o->stack));
    stack_int_push(&o->stack, o->counter);
    o->counter++;
}

struct ast_visitor graph_visitor = {
    .visit_int = graph_visit_int,
    .visit_add = graph_visit_add,
    .visit_sub = graph_visit_sub,
    .visit_mul = graph_visit_mul,
    .visit_div = graph_visit_div,
    .visit_var = graph_visit_var,
    .visit_assign = graph_visit_assign,
    .visit_statements = graph_visit_statements,
};

void graph_print_file(struct ast_node* node, FILE* out) {
    struct graph_arg arg;
    stack_int_init(&arg.stack);
    arg.outfile = out;
    arg.counter = 0;

    fprintf(out, "graph {\n");
    ast_visitor_postorder_accept(node, &graph_visitor, &arg);
    fprintf(out, "}\n");

    stack_int_destroy(&arg.stack);
}


#if YYDEBUG
extern int yydebug;
#endif

int main(int argc, const char** argv)
{
#if YYDEBUG
    yydebug = 1;
#endif
    int exit_status = 0;
    struct ast_node* node = parse_file(stdin);
    if (node != NULL)
        graph_print_file(node, stdout);
    else
        exit_status = 1;

    ast_node_destroy(node);
    return exit_status;
}
