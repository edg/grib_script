# GRIB Script

![pipeline](https://gitlab.com/edg/grib_script/badges/master/pipeline.svg)
![coverage](https://gitlab.com/edg/grib_script/badges/master/coverage.svg)

Semplice linguaggio di scripting per messaggi GRIB, ispirato a `grib_filter` e `awk`.

La struttura è la seguente:

```
BEGIN {
    // Eseguito all'inizio dello script
}

BEGINGRIB {
    // Eseguito all'inizio di ogni file GRIB
}

GRIBVALUES {
    // Eseguito per ogni valore di ogni messaggio GRIB
}

(shortName=tp && endStep=6) || startStep=0 {
    // Eseguito per ogni messaggio GRIB le cui chiavi fanno match con l'espressione booleana
}

{
    // Eseguito per ogni messaggio GRIB (espressione vuota quindi sempre vera)
}

ENDGRIB {
    // Eseguito alla fine di ogni file GRIB
}

END {
    // Eseguito alla fine dello script
}
```

La sintassi è uno pseudo-C simile ad `awk` e, oltre a tutto ciò che si porta
dietro un linguaggio in stile awk, ci saranno funzioni e variabili built-in
orientate ai GRIB. Sono da definire, ma riguarderanno:
su GRIB:

- Lettura e scrittura di chiavi del GRIB.
- Creazione di un GRIB.
- Scrittura di un GRIB.
- Operazioni per i valori dei GRIB
  - Somma, moltiplicazione, differenza, divisione tra un GRIB e un valore
    scalare.
  - Somma, moltiplicazione, differenza, divisione, mask tra due GRIB (che
    devono avere stesso grigliato)
  - Massimo, minimo di un GRIB.
- Generazione di una mappa (tramite Magics)

Ad esempio, la conversione da K a C di un GRIB di temperatura:

```
shortName=t2m {
    grib_in_c = CURRENT_GRIB - 273.15;
    write("temperatures_in_c.grib", "a", grib_in_c);
}
```


## Parser, lexer e AST

Il [lexer][1] è generato usando [Flex][2], mentre il [parser](parser.y) è
generato usando [Bison][3]. Il parser crea un [AST][4] che è rappresentato
da `struct ast_node`, che è composta da un `enum` che rappresenta il tipo di
nodo e una `union` di `struct`, una per tipo: in questo modo si riesce a
rappresentare una gerarchia di nodi dell'albero.

```
enum ast_node_type {
    INT,
    FLOAT,
     ...
};

struct ast_node {
    enum ast_node_type type;
    union {
        struct ast_node_int int_v;
        struct ast_node_float float_v;
    } node;
};
```

Il maggior problema è mantenere la consistenza tra il `type` e il
corrispondente `node`, sia in fase di creazione che di lettura del nodo. Si può
mitigare il problema in due modi:

* In fase di creazione, usando un factory method per ogni nodo: la modifica di
  un nodo comporta la modifica del solo metodo associato.
* Leggendo l'AST indirettamente con un visitor (vedi oltre), in modo da non
  dover sapere in modo implicito che ad un certo `type` corrisponde un certo
  elemento della `union` (e.g. `INT` e `int_v`), perché ogni funzione del
  visitor usa direttamente il tipo corretto (e.g. `ast_node_int`) e quindi alla
  modifica di un nodo si deve modificare solo il codice che "guida" il visitor
  all'interno di una funzione centralizzata (in realtà, sono tre, una per
  ordine di esplorazione).

## Interpretazione dell'AST

Per poter interpretare l'AST generato dal parser, viene usato un [visitor][5],
rappresentato da una `struct` che contiene un puntatore ad una funzione per ogni
tipo di nodo dell'AST. Per poter implementare un nuovo visitor, si devono
definire i puntatori alle funzioni e passare poi  AST e visitor (e una `struct`
opzionale che rappresenta lo stato del visitor) ad una delle tre funzioni
`ast_visitor_{preorder,inorder,postorder}_accept`.

Questa implementazione ha i seguenti limiti:

* Il parametro dello stato del visitor è un `void *` e questo comporta due
  problemi (che però possono essere risolti a tempo di compilazione con l'uso
  di una macro):
  * Il parametro deve essere convertito con un cast esplicito in ogni funzione
    del visitor.
  * Si deve fare attenzione a passare un tipo opportuno (e fare ancora più
    attenzione quando si cambia il tipo dopo un refactory).
* Le tre funzioni che guidano la visita hanno il noto limite relativo all'uso
  di un `case` statements. Tuttavia, l'uso di un enum insieme all'opzione
  `-Wswitch` (o ancor meglio `-Wswitch-enum`) permette di controllare a tempo di
  compilazione la dimenticanza di un caso.


## Test

Il progetto è testato usando lo unit test framework [check][6]. I test sono
eseguiti automaticamente ad ogni commit usando il Gitlab CI e sono eseguiti
usando [valgrind][7] per controllare il corretto uso della memoria. Inoltre,
viene calcolato il code coverage dei test con [lcov][8].

Si sta valutando l'uso dell'[American Fuzzy Lop][9] per testare il parser.
Tuttavia, essendo il parser generato automaticamente da una Bison, potrebbe
essere poco significativo.

[1]: lexer.l
[2]: https://github.com/westes/flex
[3]: https://www.gnu.org/software/bison/
[4]: ast.h
[5]: visitor.h
[6]: https://libcheck.github.io/check/
[7]: http://www.valgrind.org/
[8]: https://github.com/linux-test-project/lcov/
[9]: http://lcamtuf.coredump.cx/afl/

## License

Copyright (C) 2019 Emanuele Di Giacomo <emanuele@digiacomo.cc>.

Licensed under GNU GPLv2 or later.
